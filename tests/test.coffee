pjson = require '../package.json'
vows = require 'vows'
channels = require '../channels'
_ = require 'underscore'

suite = vows.describe "#{pjson.name}@#{pjson.version}"
_.each channels,(responseflag,functionname) ->
  # Only run tests for synchronous channels
  if responseflag
    suite = require("./#{functionname}")(suite)
suite.run({reporter:'spec'})

