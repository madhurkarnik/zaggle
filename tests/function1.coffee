vows = require 'vows'
function1 = require '../channels/function1'
assert = require 'assert'

module.exports = (suite) ->
  suite.addBatch
    'Tests for function1' :
      topic: () ->
        function1 {random:"shit"},@callback
      'returns best wishes' : (err,response) ->
        assert.isNull err
        assert.equal response.status,'success'

