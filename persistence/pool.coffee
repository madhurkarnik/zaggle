async = require "async"
config = require "config"
redisconf = config.redis
mysql = require "../core/mysql"
_redis = require "../core/redis"
moment = require "moment"

class PoolManagement

  @checkbalance: (callback) ->
    query = """
    SELECT poolbalance
    FROM zaggleledger
    ORDER BY id DESC
    LIMIT 1;
    """
    mysql.query query,[], (err,res) ->
      balance = Number(res[0].poolbalance) if res?.length > 0 and res[0]?.poolbalance?
      callback err, balance

  @setlock: (callback) ->
    redis = _redis.getClient()
    redis.blpop "#{redisconf.zaggle.loadlock}", 0, (err, res) ->
      redis.quit()
      callback err

  @releaselock: (callback) ->
    redis = _redis.getClient()
    redis.lpush "#{redisconf.zaggle.loadlock}", moment().unix(), (err) ->
      redis.quit()
      callback err

  @loadbalance: (data, callback) ->
    balance = null
    that = @

    setlock = (_callback) ->
      that.setlock _callback

    getbalance = (_callback) ->
      that.checkbalance (err, _balance) ->
        balance = Number(_balance) if _balance?
        _callback err

    updatebalance = (_callback) ->
      # Calculate commission and tax
      data.commission = Number((parseFloat(data.amount) * config.zaggle.commission).toFixed(2))
      data.servicetax = Number((data.commission * config.zaggle.servicetax).toFixed(2))
      _amount = Number(data.amount) - data.commission - data.servicetax

      now = moment().format("YYYY-MM-DD HH:mm:ss")
      balance += Number(_amount)
      query = """
      INSERT INTO zaggleledger (walletid, amount, poolbalance, state, txnid, created_at)
      SELECT 0, #{_amount}, poolbalance + #{_amount}, 'credited', '#{data.txnid}', '#{now}'
      FROM zaggleledger GROUP BY id DESC LIMIT 1;
      """
      mysql.query query, [], (err) ->
        _callback err

    releaselock = (_callback) ->
      that.releaselock _callback

    async.waterfall [setlock, getbalance, updatebalance, releaselock], (err) ->
      balance -= Number(config.zaggle.pooldeposit) if balance? and balance > 0
      callback err, balance

  @loaduserbalance: (user, data, callback) ->
    that = @

    updatebalance = (_callback) ->
      now = moment().format("YYYY-MM-DD HH:mm:ss")
      query = """
      INSERT INTO zaggleledger (walletid, amount, poolbalance, state, txnid, created_at)
      SELECT #{user.walletid}, #{data.amount}, poolbalance - #{data.amount}, 'debited', '#{data.txnid}', '#{now}'
      FROM zaggleledger GROUP BY id DESC LIMIT 1;
      """
      mysql.query query, [], (err) ->
        _callback err

    releaselock = (_callback) ->
      that.releaselock _callback

    async.waterfall [updatebalance, releaselock], (err) ->
      callback err

module.exports = PoolManagement
