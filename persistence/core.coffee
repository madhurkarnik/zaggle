config = require "config"
_ = require "underscore"
mysql = require "../core/mysql"

class CoreOps

  getoutletid: (_outletid,callback) ->
    outletid = null
    partnerid = config.zaggle.partnerid
    query = """
      SELECT outletid, partnerid
      FROM partneroutlets
      WHERE theiroutletid = ? AND partnerid = ?;
    """
    mysql.query query,[_outletid,partnerid],(err,res) ->
      if res?.length > 0
        outletid = res[0].outletid
        partnerid = res[0].partnerid
      else
        err = err ? "Invalid outlet id."
      callback err,outletid

  gethistory: (outletid,startdate,enddate,callback) ->
    partnerid = config.zaggle.partnerid
    query = """
      SELECT CONCAT(p.prefix,'-',p.id) AS 'payment_id', p.billnumbers AS 'bill_no',
      u.mobile, p.originalamount AS 'amount', CONCAT(r.name,', ',o.name) AS 'merchant_name',
      p.state, DATE_FORMAT(p.updated_at, "%Y-%m-%d %H:%i") AS 'updated_at'
      FROM payments p
      LEFT JOIN users u ON p.userid = u.id
      LEFT JOIN outlets o ON o.id = p.outletid
      LEFT JOIN retailers r ON r.id = p.retailerid
      WHERE p.outletid = ? AND p.partnerid = ? AND
      p.isactive='Y' AND p.deleted = 'N' AND
      DATE(p.created_at) BETWEEN '#{startdate}' AND '#{enddate}'
      ORDER BY p.updated_at desc;
    """
    mysql.query query,[outletid,partnerid],(err,res) ->
      callback err,res

  getuserhistory: (userid, walletid, callback) ->
    partnerid = config.zaggle.partnerid
    history = []
    query = """
      SELECT CONCAT(p.prefix,'-',p.id) AS 'payment_id',
      p.walletamount AS 'txn_amount',
      IF(po.theiroutletid IS NOT NULL, po.theiroutletid, '')  AS 'mcc',
      IF(p.partnerid = #{partnerid},'Zaggle','QuikWallet') AS 'source',
      IF(p.outletid IS NULL,pr.name,CONCAT(r.name,', ',o.name)) AS 'mechant_name',
      p.state, 'Debited' AS 'txn_type',
      DATE_FORMAT(p.paid_at, "%Y-%m-%d %H:%i") AS 'paid_at',
      (UNIX_TIMESTAMP(p.paid_at) * 1000) AS 'created_at'
      FROM payments p
      LEFT JOIN outlets o ON p.outletid = o.id
      LEFT JOIN retailers r ON o.retailerid = r.id
      LEFT JOIN partners pr ON p.partnerid = pr.id
      LEFT JOIN partneroutlets po ON po.outletid = p.outletid
      WHERE p.userid = #{userid} AND
      p.walletamount > 0 AND
      p.isactive = 'Y' AND p.deleted = 'N' AND
      p.state IN ('paid', 'refunded')

      UNION

      SELECT CONCAT('R-',id) AS 'payment_id',
      amount AS 'txn_amount', '' AS 'mcc',
      IF(mode = 'zaggle','Zaggle','QuikWallet') AS 'source',
      IF(mode = 'zaggle','Zaggle Wallet Load','Quikwallet Load') AS 'merchant_name',
      state, 'Credited' AS 'txn_type',
      DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") AS 'paid_at',
      (UNIX_TIMESTAMP(created_at) * 1000) AS 'created_at'
      FROM recharges
      WHERE walletid = #{walletid} AND
      isactive = 'Y' AND deleted = 'N' AND
      state = 'paid';
    """
    mysql.query query,[],(err,res) ->
      history = _.sortBy res,((txn) -> -1 * txn.created_at) if res?.length > 0
      callback err, history

  getsettlements: (date,callback) ->
    partnerid = config.zaggle.partnerid
    query = """
      SELECT r.name AS 'merchant_name',
      po.theiroutletid AS 'outlet_id', o.name AS 'outlet_name',
      po.udf1 AS 'outlet_commission', SUM(p.originalamount) AS 'outlet_daily_total',
      (SUM(p.originalamount) * (po.udf1 / 100)) AS 'outlet_daily_commission',
      (SUM(p.originalamount) - (SUM(p.originalamount) * (po.udf1 / 100))) AS 'outlet_settlement'
      FROM payments p
      LEFT JOIN outlets o ON p.outletid = o.id
      LEFT JOIN retailers r ON p.retailerid = r.id
      LEFT JOIN partneroutlets po ON po.outletid = p.outletid
      WHERE p.state IN ('paid','settlement') AND
      DATE(p.created_at) = '#{date}' AND
      p.partnerid = ?
      GROUP BY p.outletid;
    """
    mysql.query query,[partnerid],(err,res) ->
      callback err,res

module.exports = new CoreOps()
