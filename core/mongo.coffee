config = require 'config'
MongoClient = require('mongodb').MongoClient
class Mongo
  getClient:(callback,urloverride) ->
    mongourl = if urloverride? then config.mongourlplans else config.mongourl
    MongoClient.connect mongourl,callback
module.exports = new Mongo()
