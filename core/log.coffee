moment = require 'moment'
stack = require 'callsite'

class Log
  
  info:(message) ->
    if process.env.NODE_DEV
      trace = stack()[1]
      current_timestamp = moment().format('YYYY-MM-DD HH:mm:ss')
      console.log "[INFO]".grey, current_timestamp.toString().grey,trace.getFileName().toString().green,trace.getLineNumber().toString().grey,message

  error:(args...) ->
    message = ''
    for ar,i in args
      message += ar
      message += ' '
      i=i+1
    if process.env.NODE_DEV or
       process.env.NODE_ENV and process.env.NODE_ENV is 'uat' or
       process.env.NODE_ENV and process.env.NODE_ENV is 'production'
      trace = stack()[1]
      current_timestamp = moment().format('YYYY-MM-DD HH:mm:ss')
      console.log "[ERROR]".red,current_timestamp.toString().grey,trace.getFileName().toString().green,trace.getLineNumber().toString().grey,message.toString().red
    
  fatal:(args...) ->
    message = ''
    for ar,i in args
      message += ar
      message += ' '
      i=i+1
    if process.env.NODE_DEV or
       process.env.NODE_ENV and process.env.NODE_ENV is 'uat' or
       process.env.NODE_ENV and process.env.NODE_ENV is 'production'
      trace = stack()[1]
      current_timestamp = moment().format('YYYY-MM-DD HH:mm:ss')
      console.log "[FATAL]".red,current_timestamp.toString().grey,trace.getFileName().toString().green,trace.getLineNumber().toString().grey,message.toString().red
   

module.exports = new Log
