async = require "async"
redisconf = require("config").redis
_redis = require "./redis"
moment = require "moment"
log = require "../sys/log"

class ServiceLoader

  updateQueue: (callback) ->
    redis = _redis.getClient()
    multi = redis.multi()
    multi.del "#{redisconf.zaggle.loadlock}"
    multi.lpush "#{redisconf.zaggle.loadlock}",moment().unix()
    multi.exec (err) ->
      redis.quit()
      if err?
        log "Error setting up load queue.", true
      callback err

module.exports = new ServiceLoader()
