log = require ('./log')
mysqlconfig = require('config').mysql
pool = require('mysql').createPool(mysqlconfig)
mysqlexport =
  query:(query,params,callback)->
    pool.getConnection (err,conn)=>
      if err?
        log.error "[ERROR] Failed to getConnection from mySQL - #{err}"
        callback "DB error" if callback?
      conn.query query,params, (_err,rows) =>
        if _err?
          log.error _err,query,params
        conn.release()
        callback _err,rows if callback?
module.exports = mysqlexport
