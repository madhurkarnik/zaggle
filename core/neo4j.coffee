neo4j = require 'neo4j'
config = require 'config'
class Neo4J
  getdb: ->
    @neo4j ? ( @neo4j = new neo4j.GraphDatabase(config.neo4jurl))
module.exports = new Neo4J()
