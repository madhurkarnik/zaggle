async = require "async"
config = require "config"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = response = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while checking user wallet."
      syserr = "Invalid JSON #{_incoming} for checking user wallet."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.mobile? and incoming.partnerid? and incoming.signature?
      clienterr = syserr = "Missing details for checking wallet balance."
    if "#{incoming.partnerid}" isnt "#{config.zaggle.partnerid}"
      clienterr = syserr = "Invalid merchant details to check wallet balance."
    _callback clienterr

  forwardrequest = (_callback) ->
    incoming.bypassredis = true
    nats.request config.channels.users.checkwalletbalance, JSON.stringify(incoming), (err,res) ->
      response = res
      clienterr = syserr = err
      _callback clienterr

  computeloadlimit = (_callback) ->
    if response?.data? and response.data.balance? and response.data.monthlylimit?
      balancelimit = Number(config.wallet.balance.limit) - Number(response.data.balance)
      monthlylimit = Number(response.data.monthlylimit)
      loadlimit = Math.min(balancelimit, monthlylimit)
      if response.data.yearlylimit?
        yearlylimit = Number(response.data.yearlylimit)
        loadlimit = Math.min(loadlimit, yearlylimit)

      response.data.balance = Number(response.data.balance)
      response.data.loadlimit = loadlimit
    _callback null

  async.waterfall [formatincoming,validateincoming,forwardrequest,computeloadlimit], (err) ->
    if response?
      callback err,response
    else
      callback err, {status: "failed", message: "Failed to process connect request."}
