async = require "async"
config = require "config"
Pool = require "../persistence/pool"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming,callback) ->
  incoming = clienterr = syserr = balance = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while getting zaggle account balance."
      syserr = "Invalid JSON #{_incoming} for getting zaggle account balance."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and
    ((incoming.managerid? and incoming.apikey?) or
    (incoming.partnerid? and incoming.secret?))
      clienterr = syserr = "Missing details to get pool balance."
    if incoming.partnerid? and "#{incoming.partnerid}" isnt "#{config.zaggle.partnerid}"
      clienterr = syserr = "Partner authorisation failed to fetch details."
    _callback clienterr

  authenticate = (_callback) ->
    if incoming.partnerid? and incoming.secret?
      nats.request "partners/authenticate", _incoming,(err,_partner) ->
        if err? or not _partner?
          clienterr = "Partner authentication failed to fetch details."
          syserr = err ? clienterr
        _callback clienterr
    else
      _callback null

  getbalance = (_callback) ->
    Pool.checkbalance (err, _balance) ->
      if _balance?
        balance = (Number(_balance) - Number(config.zaggle.pooldeposit)).toFixed(2)
      if err?
        clienterr = "Error fetching balance details."
        syserr = err
      _callback clienterr

  async.waterfall [formatincoming,validateincoming,authenticate,getbalance], (err) ->
    if err?
      log syserr, true
      callback clienterr, {status: "failed", message: clienterr}
    else
      callback null, {status: "success", data: {balance: balance, threshold: config.zaggle.poolthreshold}}
