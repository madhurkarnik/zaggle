async = require "async"
moment= require "moment"
Core = require "../persistence/core"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = history = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while getting outlet history."
      syserr = "Invalid JSON #{_incoming} for getting outlet history."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.partnerid? and incoming.secret? and incoming.outletid?
      clienterr = syserr = "Missing details to get payment history for outlet."
    if not incoming.startdate? or "#{incoming.startdate}".trim() is ''
      incoming.startdate = moment().format("YYYY-MM-DD")
    if not incoming.enddate? or "#{incoming.enddate}".trim() is ''
      incoming.enddate = moment().format("YYYY-MM-DD")
    _callback clienterr

  authenticate = (_callback) ->
    nats.request "partners/authenticate",_incoming, (err,partner) ->
      clienterr = syserr = err ? "Merchant authentication failed." if not partner?
      _callback clienterr

  getoutletid = (_callback) ->
    Core.getoutletid incoming.outletid,(err,_outletid) ->
      incoming.outletid = _outletid if _outletid?
      if err? or not _outletid?
        clienterr = "Invalid merchant id to get outlet history."
        syserr = err ? clienterr
      _callback clienterr

  gethistory = (_callback) ->
    Core.gethistory incoming.outletid,incoming.startdate,incoming.enddate,(err,_payments) ->
      if err?
        clienterr = "Failed to get payment history for outlet."
        syserr = err
      history = _payments
      _callback clienterr

  async.waterfall [formatincoming,validateincoming,getoutletid,authenticate,gethistory], (err) ->
    if err?
      log syserr,true
      callback clienterr, {status: "failed", message: clienterr}
    else
      callback null, {status: "success", data: {history: history}}
