async = require "async"
config = require "config"
_ = require "underscore"
moment = require "moment"
excelparser = require "excel-parser"
fs = require "fs-extra"
responsehelper = require "../helpers/onboardingresponse"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming,callback) ->
  incoming = JSON.parse(_incoming)
  filedata = incoming.files
  filepath = filedata.path
  filename = incoming.filename ? filepath.substring(filepath.lastIndexOf('/') + 1)
  entries = []
  parsedworksheet = filehash = null

  #check for incoming data
  validateincoming = (_callback)->
    unless filedata?
      _callback "Missing important data."
    else
      _callback null

  # Parse the xls file into a json.
  parseworksheet = (_callback) ->
    excelparser.parse {inFile: filepath, worksheet: 1}, (err, worksheet) ->
      if err?
        log "Bulk upload parsing failed: #{err}"
        _callback err
      else
        parsedworksheet = worksheet
        _callback null

  # Call request payment helper for entries in the xls file.
  onboardoutlets = (_callback) ->
    counter = 0
    if parsedworksheet.length > 0
      _callback null
      emailRegex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
      async.eachSeries parsedworksheet,((row,__callback)->
        counter++
        merchant = {}
        merchant.onboardedby = incoming.managerid
        if row?
          # Validating affiliate name
          merchant.affiliatename = "#{row[0]}".trim()
          unless merchant.affiliatename isnt ''
            err = "Invalid affiliate name for onboarding a merchant, #{merchant.affiliatename}"
          # Validating outlet code
          merchant.outletcode = "#{row[1]}".trim()
          unless merchant.outletcode isnt ''
            err = "Invalid outlet code for onboarding a merchant, #{merchant.outletcode}"
          # Validating outlet name
          merchant.outletname = "#{row[2]}".trim()
          unless merchant.outletname isnt '' and merchant.outletname.length <= 50
            err = "Invalid outlet name for onboarding a merchant, #{merchant.outletname}"
          # Validating outlet mobile
          merchant.outletmobile = "#{row[3]}".trim()
          unless merchant.outletmobile isnt '' or merchant.outletmobile.length is 10 and not isNaN(merchant.outletmobile)
            err = "Invalid outlet mobile number for onboarding a merchant, #{merchant.outletmobile}"
          # Validating QR code
          qrcode = "#{row[4]}".trim().toUpperCase()
          merchant.qrcode = if qrcode isnt '' then qrcode else null
          # Validating terminal mobile. Accepting maximum of 10(comma seprated) mobile numbers
          merchant.terminalmobile = "#{row[5]}".trim()
          unless merchant.terminalmobile isnt '' and merchant.terminalmobile.length is 10 and not isNaN(merchant.terminalmobile)
            err = "Invalid terminal mobile number for onboarding a merchant."
          # Validating email
          emailRegex = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
          merchant.email = "#{row[6]}".trim()
          unless emailRegex.test(merchant.email)
            err = "Invalid email id for merchant."
          # Validate commission rate
          merchant.commission = "#{row[7]}".trim()
          unless merchant.commission isnt '' and not isNaN(merchant.commission) and merchant.commission >= 0
            err = "Invalid merchant commission rate."

          if not err?
            nats.request "outlets/createzagglemerchant",JSON.stringify(merchant),(err) ->
              if err?
                merchant.error = err
                log "Onboarding failed for #{merchant.affiliatecode}, #{merchant.outletcode}, #{merchant.tid}.",true
              entries.push merchant
              setTimeout (=>
                clearTimeout()
                __callback(null)
              ), 500
          else if counter is 1
            __callback null # This is an exception assuming the first row will be headers
          else
            merchant.error = err
            entries.push merchant
            log "Onboarding failed for merchant: #{err}",true
            __callback null
        else
          __callback null # This is an exception assuming the row is an empty string
      ),(err) ->
        responsehelper.sendresponse entries,filename
        #_callback null
    else
      _callback 'Empty File'

  async.waterfall [validateincoming,parseworksheet,onboardoutlets], (err) ->
    log "[FIN] Merchant onboarding for #{moment().format("YYYY-MM-DD")} completed"
    if err?
      log err,true
      callback err,{'status':'failed'}
    else
      callback null, {'status':'success'}
