async = require "async"
config = require "config"
moment= require "moment"
Core = require "../persistence/core"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = userid = walletid = history = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while getting outlet history."
      syserr = "Invalid JSON #{_incoming} for getting outlet history."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.mobile? and incoming.partnerid? and incoming.signature?
      clienterr = syserr = "Missing details to fetch user history."
    if incoming.partnerid? and "#{incoming.partnerid}" isnt "#{config.zaggle.partnerid}"
      clienterr = syserr = "Invalid merchant details to fetch user history."
    _callback clienterr

  authenticate = (_callback) ->
    nats.request "users/authenticate", _incoming, (err, _user) ->
      userid = _user?.id
      walletid = _user?.walletid
      if err? or not _user?
        clienterr = "User authentication failed. PLease try again."
        syserr = err ? clienterr
      _callback clienterr

  gethistory = (_callback) ->
    Core.getuserhistory userid, walletid, (err,_history) ->
      history = _history
      if err?
        clienterr = "Error while fetching user history."
        syserr = err
      _callback clienterr

  async.waterfall [formatincoming, validateincoming, authenticate, gethistory], (err) ->
    if err?
      log syserr, true
      callback clienterr, {status: "failed", message: clienterr}
    else
      callback null, {status: "success", data: history}
