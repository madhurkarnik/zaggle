async = require "async"
config = require "config"
channelconf = config.channels
Core = require "../persistence/core"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = user = response = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while attempting the payment."
      syserr = "Invalid JSON #{_incoming} for attempting the payment."
    finally
      _callback clienterr

  validateincoming =  (_callback) ->
    unless incoming? and ((incoming.amount? and incoming.outletid?) or incoming.id?) and
    incoming.mobile? and incoming.partnerid? and incoming.signature?
      clienterr = syserr = "Missing details to attempt a payment."
    _callback clienterr

  getoutletid = (_callback) ->
    Core.getoutletid incoming.outletid,(err,_outletid) ->
      if _outletid?
        incoming.outletid = _outletid
      else
        clienterr = syserr = "Invalid merchant details to attempt a payment."
      _callback clienterr

  forwardrequest = (_callback) ->
    incoming.bypassredis = true
    incoming.bypassgateway = true
    incoming.paymentmode = config.paymentmodes.zaggle
    data =
      reqdata: incoming
      type: null
      ipaddress: incoming.ipaddress
      legacymode: false
    nats.request "transactions/process", JSON.stringify(data), (err,res) ->
      response = res
      _callback err

  async.waterfall [formatincoming,validateincoming,getoutletid,forwardrequest], (err) ->
    if response?
      callback err,response
    else
      callback err, {status: "failed", message: clienterr ? "Failed to process connect request."}
