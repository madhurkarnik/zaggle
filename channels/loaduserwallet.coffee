async = require "async"
config = require "config"
Pool = require "../persistence/pool"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = user = null
  loadlock = false

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while loading user wallet."
      syserr = "Invalid JSON #{_incoming} for loading user wallet."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.amount? and incoming.amount > 0 and
    incoming.mobile? and incoming.partnerid? and incoming.signature?
      clienterr = syserr = "Missing details to load user wallet."
    _callback clienterr

  getuser = (_callback) ->
    incoming.bypassredis = true
    nats.request "users/authenticate", JSON.stringify(incoming), (err,_user) ->
      user = _user if _user?
      if err? or not user?
        clienterr = "User authentication failed."
        syserr = err
      _callback clienterr

  verifyconnect = (_callback) ->
    data =
      user: user
      userid: user.id
      partnerid: incoming.partnerid
      mobile: incoming.mobile
    nats.request "partners/verifywalletconnect", JSON.stringify(data), (err,isconnected) ->
      if err? or isconnected isnt true
        clienterr = "User authentication failed."
        syserr = err ? clienterr
      _callback clienterr

  checkpoolbalance = (_callback) ->
    Pool.checkbalance (err,_balance) ->
      _balance = Number(Number(_balance) - config.zaggle.pooldeposit).toFixed(2) # Removing deposit amount from balance
      newbalance = Number(_balance) - Number(incoming.amount) if _balance?
      if _balance? and newbalance <= config.zaggle.poolthreshold
        clienterr = syserr = "Pool balance at threshold. Please recharge to resume loading user wallets."
      if err?
        clienterr = "Error fetching available pool balance."
        syserr = err
      _callback clienterr

  setloadlock = (_callback) ->
    Pool.setlock (err) ->
      if err?
        clienterr = "Error updating user wallet balance."
        syserr = err
      else
        loadlock = true
      _callback clienterr

  updatewallet = (_callback) ->
    data =
      reqdata:
        metacardid: config.universalmetacards.rbicard.id
        bypassredis: true
        mobile: incoming.mobile
        partnerid: incoming.partnerid
        signature: incoming.signature
        walletid: user.walletid ? null
        amount: incoming.amount
        bypassgateway: true
        mode: config.paymentmodes.zaggle
      legacymode: true
    nats.request "transactions/process", JSON.stringify(data),(err,res) ->
      _resp = res.data
      if not err? and _resp?
        user.walletid = _resp.walletid
        incoming.rechargeid = "R-#{_resp.paymentid ? _resp.rechargeid}"
      else
        clienterr = syserr = err ? "Error updating user wallet balance."
      _callback clienterr

  updateledger = (_callback) ->
    data =
      amount: incoming.amount
      txnid: incoming.rechargeid
    Pool.loaduserbalance user, data, (err) ->
      if err?
        clienterr = "Error updating user wallet balance."
        syserr = err
      else
        loadlock = false
      _callback clienterr

  releaselock = () ->
    log "Explicit load lock release called"
    Pool.releaselock (err) ->
      log "Explicit load lock release failed",true if err?

  async.waterfall [formatincoming,validateincoming,getuser,verifyconnect,checkpoolbalance,setloadlock,updatewallet,updateledger], (err) ->
    if loadlock
      releaselock()
    if err?
      log syserr,true
      callback clienterr, {status: "failed", message: clienterr}
    else
      callback null, {status: "success", message: "User wallet updated successfully.", data: {transaction_id: "#{incoming.rechargeid}"}}
