async = require "async"
config = require "config"
Pool = require "../persistence/pool"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = syserr = clienterr = balance = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while loading pool balance."
      syserr = "Invalid JSON #{_incoming} for loading pool balance."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.managerid? and incoming.apikey? and
    incoming.amount? and incoming.amount > 0 and incoming.txnid?
      clienterr = syserr = "Missing important details to load pool balance."
    _callback clienterr

  updatepoolbalance = (_callback) ->
    Pool.loadbalance incoming,(err,_balance) ->
      balance = Number(_balance.toFixed(2)) if _balance > 0
      if err?
        clienterr = "Something went wrong while updating Zaggle's pool balance."
        syserr = err
      _callback clienterr

  async.waterfall [formatincoming,validateincoming,updatepoolbalance], (err) ->
    if err?
      log syserr,true
      callback clienterr, {status: "failed", message: clienterr}
    else
      log "Zaggle account loaded with Rs. #{incoming.amount}. Updated balance is Rs. #{balance}"
      callback null, {status: "success", data: {balance: balance}}
