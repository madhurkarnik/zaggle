async = require "async"
config = require "config"
nats = require "../sys/nats"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = response = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while loading user wallet."
      syserr = "Invalid JSON #{_incoming} for loading user wallet."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.otp? and incoming.kyctype? and incoming.kycvalue? and
    incoming.mobile? and incoming.partnerid? and incoming.signature?
      clienterr = syserr = "Missing details to connect wallet."
    if "#{incoming.partnerid}" isnt "#{config.zaggle.partnerid}"
      clienterr = syserr = "Invalid merchant details to connect wallet"
    _callback clienterr

  forwardrequest = (_callback) ->
    incoming.bypassredis = true
    nats.request config.channels.partners.connectuserwallet, JSON.stringify(incoming), (err,res) ->
      response = res
      clienterr = syserr = err
      _callback clienterr

  async.waterfall [formatincoming,validateincoming,forwardrequest], (err) ->
    if response?
      callback err,response
    else
      callback err, {status: "failed", message: "Failed to process connect request."}
