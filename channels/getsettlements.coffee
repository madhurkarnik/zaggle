async = require "async"
moment = require "moment"
_ = require "underscore"
Core = require "../persistence/core"
nats = require "../sys/nats"
log = require "../sys/log"

module.exports = (_incoming, callback) ->
  incoming = clienterr = syserr = settlements = null

  formatincoming = (_callback) ->
    try
      incoming = JSON.parse _incoming
    catch e
      clienterr = "Something went wrong while getting settlements."
      syserr = "Invalid JSON #{_incoming} for getting settlements."
    finally
      _callback clienterr

  validateincoming = (_callback) ->
    unless incoming? and incoming.partnerid? and incoming.secret?
      clienterr = syserr = "Missing details to get payment settlements."
    if incoming.date? and "#{incoming.date}".trim() isnt ''
      incoming.date = moment(incoming.date).format("YYYY-MM-DD")
    else
      incoming.date = moment().subtract(1,"days").format("YYYY-MM-DD")
    _callback clienterr

  authenticate = (_callback) ->
    nats.request "partners/authenticate",_incoming, (err,partner) ->
      clienterr = syserr = err ? "Merchant authentication failed." if not partner?
      _callback clienterr

  getsettlements = (_callback) ->
    Core.getsettlements incoming.date,(err,_payments) ->
      clienterr = syserr = err
      settlements = _payments
      _callback clienterr

  addids = (_callback) ->
    counter = 1
    if settlements.length > 0
      _.each settlements,(entry) ->
        if entry?
          entry.settlement_date = incoming.date
          entry.settlement_id = "#{incoming.date}-#{counter}"
          entry.outlet_daily_commission = entry.outlet_daily_commission.toFixed(2)
          entry.outlet_settlement = entry.outlet_settlement.toFixed(2)
          counter++
    _callback null

  async.waterfall [formatincoming,validateincoming,authenticate,getsettlements,addids], (err) ->
    if err?
      log syserr,true
      callback clienterr, {status: "failed", message: clienterr}
    else
      callback null, {status: "success", data: {settlements: settlements}}
