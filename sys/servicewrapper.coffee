async = require 'async'
nats = require('nats').connect('http://nats:4222')
channels = require '../channels'
fullchannels = require '../fullchannels'
pjson = require '../package.json'
_ = require 'underscore'
log = require './log'

module.exports = () ->
  synchronouschannels = []
  asynchronouschannels = []

  _.each channels,(responseflag,functionname) ->
    channel =
      name : "#{pjson.name}/#{functionname}"
      filename : "#{functionname}"
    if responseflag is true
      synchronouschannels.push channel
    else
      asynchronouschannels.push channel

  _.each fullchannels, (channel,channelname) ->
    channel.name = "#{channelname}"
    if channel.synchronous is true
      synchronouschannels.push channel
    else
      asynchronouschannels.push channel

  async.each synchronouschannels,(channel,_callback) ->
    nats.subscribe "#{channel.name}",{queue:"#{channel.name}"},(request,replyto) ->
      func = require "../channels/#{channel.filename}"
      func request,(err,response,broadcast) ->
        _output =
          err: err
          data: response
          request: request
          broadcast: broadcast
        output = JSON.stringify _output
        nats.publish replyto,output
        if not err?
          nats.publish "completed.#{channel.name}",output
        else
          nats.publish "fukup.#{channel.name}",output
          log err

  async.each asynchronouschannels,(channel,_callback) ->
    nats.subscribe "#{channel.name}",(message) ->
      require("../channels/#{channel.filename}")(input,(err,broadcast) ->
        _output =
          err: err
          request: input
          broadcast: broadcast
        output = JSON.stringify _output
        if not err?
          nats.publish "completed.#{channel.name}",output
        else
          nats.publish "fukup.#{channel.name}",output
          log err
      )

  log "Zaggle Service Up on #{new Date()} with node version #{process?.version}".bold.green

process.on 'SIGINT',  ()->
  require('./die')('SIGINT')
