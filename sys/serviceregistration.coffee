async = require 'async'
_redis = require 'redis'
pjson = require '../package.json'
log = require './log'

module.exports = () ->
  
  # Add entry in redis active hash
  addtoredis = (_callback) ->
    redis = _redis.createClient(6379,'redis',{})
    multi = redis.multi()
    multi.hset "qw:services:active",pjson.name,pjson.version
    multi.sadd "qw:services",pjson.name
    multi.exec (err) ->
      redis.quit()
      _callback err
  
  # Broadcast event in NATS
  publish = (_callback) ->
    nats = require('nats').connect('http://nats:4222')
    nats.publish "system.service.up.#{pjson.name}",pjson.version,() ->
      nats.close()
      _callback null

  async.series [addtoredis,publish],()->
    log "Zaggle Service Up"
