pjson = require '../package.json'
semver = require 'semver'
nats = require('nats').connect('http://nats:4222')
module.exports = () ->
  nats.subscribe 'system.survey',{queue:"system.survey/#{pjson.name}"},(message,replyto) ->
    data =
      name: pjson.name
      version: pjson.version
      uptime: process.uptime()
    nats.publish replyto,JSON.stringify(data)
  nats.subscribe "system.service.up.#{pjson.name}",(version) ->
    if semver.gt(version,pjson.version)
      require('./die')("upgrade to #{version}")

