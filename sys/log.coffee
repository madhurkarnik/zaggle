require 'colors'
pjson = require '../package.json'
module.exports = (message,critical=false) ->
  prefix = if critical then "[ERROR]" else "[INFO]"
  if critical
    console.log "[#{pjson.name}@#{pjson.version}] #{prefix} #{message}".red
  else
    console.log "[#{pjson.name}@#{pjson.version}] #{prefix} #{message}"
