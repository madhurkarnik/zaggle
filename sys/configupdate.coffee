log = require './log'
module.exports = () ->
  nats = require('nats').connect('http://nats:4222')
  nats.subscribe 'system.config.update',(_config) ->
    global.config = JSON.parse(_config)
    log "Updating config to version #{global.config.version}"

