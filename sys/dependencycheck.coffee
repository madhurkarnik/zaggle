_redis = require 'redis'
_ = require 'underscore'
dependencies = require './dependencies'
semver = require 'semver'

module.exports = (_callback) ->
  fukup = null

  #Check redis for dependencies
  if _.isEmpty dependencies
    _callback null
  else
    dependencynames = _.keys dependencies
    requiredversions = _.values dependencies
    redis = _redis.createClient(6379,'redis',{})
    redis.hmget "qw:services:active",dependencynames,(err,activeversions) ->
      redis.quit()
      if not err?
        fukedservice = null
        _.each requiredversions,(requiredversion,index) ->
          if not activeversions[index]? or not semver.gte(activeversions[index],requiredversion)
            fukedservice = "#{dependencynames[index]}@#{requiredversion}"
        if fukedservice?
          fukup =  "Missing dependency : #{fukedservice}"
          _callback fukup
        else
          _callback null
      else
        fukup =  "System Redis fucked"
        _callback fukup
