nats = require('nats').connect('http://nats:4222')

module.exports =

  #Request as JSON
  request : (channel,_request,callback) ->
    nats.request channel,_request,(_response) ->
      response = JSON.parse _response
      err = response.err
      callback err,response.data

  #Request as stringified JSON
  requeststringified : (channel,_request,callback) ->
    nats.request channel,_request,(_response) ->
      response = JSON.parse _response
      err = response.err
      callback err,JSON.stringify(response.data)

  #Publish
  publish : (channel,_message) ->
    nats.publish channel,_message
