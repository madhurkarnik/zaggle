log = require "./sys/log"

if (process.env.NODE_ENV != null && process.env.NODE_ENV == 'production')
  pmx = require('pmx').init()
# Start service
require('./sys/servicewrapper')()

require('./core/bootstrap').updateQueue (err) ->
  if err?
    log "Error setting redis queue! #{err}",true
  else
    log "Redis queue set successfully."
