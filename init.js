require('coffee-script/register');
var log = require("./sys/log")
var pmx;

if (process.env.NODE_ENV !== null && process.env.NODE_ENV === 'production') {
    pmx = require('pmx').init();
}

require('./sys/servicewrapper')();

require('./core/bootstrap').updateQueue(function(err) {
  if (err != null) {
    return log("Error setting redis queue! " + err, true);
  } else {
    return log("Redis queue set successfully.");
  }
});
