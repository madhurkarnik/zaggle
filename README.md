## NodeJS QuikService Boilerplate ##

This is a boilerplate repo from which you are to fork for implementing a microservice in Node.js. This already has the following npm modules installed :

 - AsyncJS
 - NATS
 - Redis and HiRedis
 - UnderscoreJS
 - Semver
 - Colors
 - VowsJS
 
Additional modules if needed can be installed in the service fork.After forking, please *remove* all this *generic documentation* to supply service specific documentation. 

### [Start] Generic Documentation ###

#### Service basics ####
Each service will have a name (dns-like) and version, which is to be specified in the `package.json` file. This is very important as it will be used for service registration and dependency management.
Example services :

    persistence.user.offers@1.0.0
    helpers.user.checkin@1.0.9
    ...
    
#### Dependency check ####

Each service needs to have its dependencies (if any) specified in `dependencies.coffee` as key (service name) and value(version).
During service initialization, the service will first check if all dependencies are active in a redis hash `qw:services:active` , which contains the same key-value combination,i.e. service name and version - for all currently *running* services.

#### Service Registration ####
If all dependencies are satisfied (i.e. Redis says they are up and running), the current service will proceed to register itself with redis - so other services which may be dependent on this can declare dependency in a similar fashion.

#### Config Loading ####
#### Listening for Config changes ####
#### Survey / Health Check ####
#### Upgrade ####
#### Channels (Remote Procedures) ####

NATS is the messaging system the distributed architecture will use for communication. There are two types of communication patterns for a service :

 1. Request Reply (Synchronous)
 2. Pub Sub (Asynchronous/Reactive)
 
Each service can have one or more *channels* (the equivalent of functions/methods in a monolithic architecture) which is either of the above types. The channel names should be the servicename/functionname.
As an example, the service `helpers.user.verify` exposes multiple related channels such as

    helpers.user.verify/verify
    helpers.user.verify/verifyotp
    helpers.user.verify/twofactorlogin

Thus the channels are basically remote procedures which look like a URL
`[servicenamedns]/[functionname]`

It is possible for multiple services to listen on the same channel - with some choosing not to respond (i.e. asynchronous / reactive functions).

You need to enlist function names in `channels.coffee`. This is an object with key being function name an value being a boolean which indicates whether the channel is synchronous (replies) or not(reacts). 
Example :

    "function1" : true
    "function2" : false

In addition to this, you need to have each channel's implementation in the `channels/` folder with the same name as the function itself. Thus , for the above functions :

    channels/function1.coffee
    channels/function2.coffee

The boilerplate's service wrapper will automatically do the needful. 

#### Testing ####

We use Vows.js for writing tests for every synchronous service (i.e. whose `responseflag` is true in `channels.coffee`. At this point, we are not doing automated tests for asynchronous/reactive channels.

The tests folder is `tests/` which always has the test runner `testrunner.coffee`. You are expected to create Vow tests for every synchronous channel with the same name.
Thus for the above example,

    tests/function1.coffee

Note that `tests/function2.coffee` does not exist in the boilerplate as it is asynchronous.

### [END] Generic Documentation ###

What will be unique to each service , after its forked from this boilerplate is 
