async = require "async"
config = require "config"
_ = require "underscore"
moment = require "moment"
fs = require "fs"
excelbuilder = require 'msexcel-builder'
nodemailer = require "nodemailer"
Core = require "../persistence/core"
log = require "../sys/log"

class OnboardingResponse

  sendresponse: (rows,filename,callback) ->
    workbook = merchantsheet = null
    populateheaders = (_callback) ->
      workbook = excelbuilder.createWorkbook("/tmp", "Response_#{filename}")
      merchantsheet = workbook.createSheet("Response", 15, (rows.length + 5))
      # Static content
      merchantsheet.set 1,1,"merchant_name"
      merchantsheet.font 1,1,{bold:'true'}
      merchantsheet.set 2,1,"outlet_id"
      merchantsheet.font 2,1,{bold:'true'}
      merchantsheet.set 3,1,"outlet_name"
      merchantsheet.font 3,1,{bold:'true'}
      merchantsheet.set 4,1,"outlet_mobile_number"
      merchantsheet.font 4,1,{bold:'true'}
      merchantsheet.set 5,1,"qr_code"
      merchantsheet.font 5,1,{bold:'true'}
      merchantsheet.set 6,1,"terminal_mobile_number"
      merchantsheet.font 6,1,{bold:'true'}
      merchantsheet.set 7,1,"email"
      merchantsheet.font 7,1,{bold:'true'}
      merchantsheet.set 8,1,"outlet_commission"
      merchantsheet.font 8,1,{bold:'true'}
      merchantsheet.set 9,1,"status"
      merchantsheet.font 9,1,{bold:'true'}
      merchantsheet.set 10,1,"reason"
      merchantsheet.font 10,1,{bold:'true'}
      _callback null

    populatecontent = (_callback) ->
      counter = 2
      _.each rows,(entry) ->
        merchantsheet.set 1,counter,"#{entry.affiliatename}"
        merchantsheet.set 2,counter,"#{entry.outletcode}"
        merchantsheet.set 3,counter,"#{entry.outletname}"
        merchantsheet.set 4,counter,"#{entry.outletmobile}"
        merchantsheet.set 5,counter,"#{entry.qrcode}"
        merchantsheet.set 6,counter,"#{entry.terminalmobile}"
        merchantsheet.set 7,counter,"#{entry.email}"
        merchantsheet.set 8,counter,"#{entry.commission}"
        merchantsheet.set 9,counter,"#{if entry.error? then "Failed" else "Success"}"
        merchantsheet.set 10,counter,"#{entry.error ? ''}"
        counter++
      _callback null

    savefile = (__callback) ->
      workbook.save (_err) ->
        if _err?
          workbook.cancel()
          __callback "Excel workbook save failed"
        else
          log "[ACK] Onboarding report - Excel sheet generated"
          __callback null

    sendfile = (_callback) ->
      transport = nodemailer.createTransport "SES",
        AWSAccessKeyID: config.ses.id
        AWSSecretKey: config.ses.key
      fs.readFile "/tmp/Response_#{filename}",(err,data) ->
        if not err?
          message =
            to: config.zaggle.email
            from: "QuikWallet <noreply@quikwallet.com>"
            subject: "Onboarding report for #{moment().format("YYYY-MM-DD")}"
            body: "PFA reports ...\n   #{process.env.NODE_ENV} Environment."
            attachments: [
              filename: "Response_#{filename}"
              contents: data
            ]
          transport.sendMail message, (error) ->
            if error?
              log "AMAZON SES failed with #{error}",true
            else
              log "[ACK] Onboarding merchants for #{moment().format('YYYY-MM-DD')}"
            _callback error
        else
          log "Email failed - error reading report file contents: #{err}",true
          _callback err

    async.waterfall [populateheaders,populatecontent,savefile,sendfile],callback


module.exports = new OnboardingResponse()
